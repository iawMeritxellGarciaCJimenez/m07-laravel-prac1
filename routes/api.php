<?php
namespace App\Models;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\UserCollection;
use App\Http\Resources\ProductResource;
use App\Http\Resources\UserResource;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/users', function () {
    return new UserCollection(User::all());
});

//Listar todos los productos
Route::get('/products', function() {
    return new ProductCollection(Product::all());
});

//Listar un producto por ID
Route::get('/product/{id}', function($id) {
    return new ProductResource(Product::findOrFail($id));
});

//Crear un nuevo producto
Route::post('/product', [ProductController::class, 'store']);

//Modificar un product
Route::put('/product/{id}', function(Request $request, $id) {
    $product = Product::whereId($id)->first()->update($request->all());
    return Product::find($id);
});

//Eliminar un producto
Route::delete('/product/{id}', function($id) {
    $product = Product::find($id);
    $product->delete();
});

//Crear un nuevo usuario
Route::post('/usuario', [ProductController::class, 'newUser']);


//Modificar un usuario
Route::put('/usuario/{name}', function(Request $request, $name) {
    $user = User::whereId($name)->first()->update($request->all());
    return User::find($name);
});

//Eliminar un usuario
Route::delete('/usuario/{name}', function($name) {
    $user = User::find($name);
    $user->delete();
});